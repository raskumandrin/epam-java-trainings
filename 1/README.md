Компиляция и запуск приложения из командной строки
==================================================

Задание
-------

Дан код:

_Logic.java_

```java
package javase01.t01.logic;

public class Logic {
    public String method(){
        return "I am string in Logic.";
    }

}
```

_Main.java_

```java
package javase01.t01.main;

import javase01.t01.logic.Logic;

public class Main {
    public static void main(String[] args) {
        Logic logic = new Logic();
        System.out.println(logic.method());
    }
}
```

Необходимо скомпилировать и запустить данный код из консоли.

Решение
-------

Файлы проекта расположены в `~/Desktop/java/javase01/t01` (это же является текущим каталогом)


Компиляция

```
javac -cp ../.. main/Main.java 
```

Запуск

```
java -cp ../.. javase01/t01/main/Main
```
