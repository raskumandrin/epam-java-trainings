public class Main {
    public static void main(String[] args) {
        double a = Double.parseDouble(args[0]);
        double b = Double.parseDouble(args[1]);
        double h = Double.parseDouble(args[2]);

        do {
            double f = Math.tan(2*a) - 3;
            System.out.println( "" + a + ": " + f);
        }
        while ( ( a += h ) <= b );
    }
}

