public class Main {
    public static void main(String[] args) {
        int i = 1;
        double element = 0.0;
        do {
            element = ( 1 / java.lang.Math.pow( i + 1, 2 ) );
            System.out.println("a[" + i + "] = " + element);
            i++;
        }
        while ( element >= Double.parseDouble(args[0]) );
    }
}

