public class Main {
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        int dim[][] = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                dim[i][j] = ( (i==j || N-i==j+1) ? 1 : 0 );
            }
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print(dim[i][j] + " ");
            }
            System.out.println();
        }
    }
}

